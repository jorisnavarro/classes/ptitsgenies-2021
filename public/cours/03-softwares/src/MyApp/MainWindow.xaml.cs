﻿using System.Windows;
using Newtonsoft.Json;
using System.Net.Http;

namespace MyApp
{
    public partial class MainWindow : Window
    {
        const string apikey = "eacc00e4bc876095f35653d0fb65e118";
        private static readonly HttpClient client = new HttpClient();

         public MainWindow()
        {
            InitializeComponent();
        }
        private async void ButtonMeteo_Click(object sender, RoutedEventArgs e)
        {
            var msg = await client.GetStringAsync($"https://api.openweathermap.org/data/2.5/weather?q={City.Text}&appid={apikey}&lang=fr&units=metric");

            OpenWeatherMapResponse resultat = JsonConvert.DeserializeObject<OpenWeatherMapResponse>(msg);

            Resultat.Content = $"{resultat.main.temp} °C, {resultat.weather[0].description}";
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
