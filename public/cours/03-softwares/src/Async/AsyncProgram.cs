﻿using System;
using System.Threading.Tasks; 
using System.Net.Http;
using Newtonsoft.Json;

namespace Async
{
    class AsyncProgram    
    {
        private static readonly HttpClient client = new HttpClient();
        const string apikey = "eacc00e4bc876095f35653d0fb65e118";
        static async Task Main(string[] args)
        {
            string cityName = "Le Soler";
            if (args.Length > 0)
             cityName = args[1];

           await ProcessRepositories(cityName);
        }
      

        private static async Task ProcessRepositories(string cityName)
        {
            Console.WriteLine($"Vérifions la météo de {cityName} sur OpenWeatherMap ...");
            var msg = await client.GetStringAsync($"https://api.openweathermap.org/data/2.5/weather?q={cityName}&appid={apikey}&lang=fr&units=metric");

            OpenWeatherMapResponse root = JsonConvert.DeserializeObject<OpenWeatherMapResponse>(msg);
            Console.WriteLine($"{root.main.temp} °C, {root.weather[0].description}");
        }
    }
}
