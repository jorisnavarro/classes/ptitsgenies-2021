# 00 Elecronique

## Séquence de formation

Electronique

## Durée 

7h

## Ojectif

* Connaitre les bases de l'électonique, 
* mesurer une tension continue, 
* connaitre les opérateurs logique, 
* comprendre la logique d'un microcontrolleur

## contenu


## Matériel

Kit arduino de base

## Effectué

Non

## Déroulé

on part de zéro

[les bases](https://fr.flossmanuals.net/arduino/les-bases-de-lelectronique/)

La différence de potentel (tension) U en Volts
Le courant (intensité) I en Ampère
La résistance (rétressement) R en Ohm

circuit ouvert ou fermé
série ou parallèle

AC/DC
Fréquence 230V 50hz (période)




composants passifs et actifs



## Notes
[https://wwww.circuits.io/app](https://wwww.circuits.io/app)

[Introduction aux systèmes informatiques](https://www.iro.umontreal.ca/~monnier/1215/notes-logic.pdf)

[COurs Architecture des ordinateurs](https://djerroud.halim.info/wp-content/uploads/2018/05/Cours2.pdf)