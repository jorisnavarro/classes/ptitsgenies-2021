https://fr.wikipedia.org/wiki/Programme_informatique


```properties
dotnet new console --name First
```

```properties
cd First
```
```properties
dotnet add package Newtonsoft.Json
```
```properties
dotnet run
```

 dotnet new wpf --name MyApp

### Format
* script (montrer un script bat)
* console (montrer une appli console)
* services (montrer un service)
* desktop (montrer un app)
* web (montrer une web app)
* mobile (montrer une app mobile)

### Type de programme
* utilitaires
* Transversaux
* métiers

Les programmes informatiques sont concernés par le droit d'auteur et font l'objet d'une législation proche des œuvres artistiques.

### licences
* opensource (MIT, AGPL, ...)
* creative commons
* propriétaire

### model éco
* Freeware
* shareware
* saas

* compilé, interprété => pour le cours code

On-premise

### installation/désinstallation
* portable
* setup
* standalone (jeux ?)
* distribué (jeux ?)

### sécutité
* droits (heritage droits utilisateurs)
* pare-feu
* virus/backdoor

### Logiciel intégré

* Windows Defender
* Parefeu
* Teams
* Skype

### Exemple


* [AdwCleaner](https://fr.malwarebytes.com/adwcleaner/)
* [VLC](https://www.videolan.org)
* [Foxit PDF Reader](https://www.foxit.com/fr/pdf-reader/)
* [VirtualBox](https://www.virtualbox.org/)
* [Whatsapp](https://www.whatsapp.com/download/)
* [keepass](https://keepass.fr/)
* [ShareX](https://getsharex.com/)
* [Calibre](https://calibre-ebook.com/fr/download)
* [BlueStack](https://www.bluestacks.com/fr/)
* [PowerToys](https://docs.microsoft.com/fr-fr/windows/powertoys/install)
* [SysInternal Suite](https://docs.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite)
* [Spotify](https://www.spotify.com/fr/download/windows/)
* [PC HealthCheck](https://www.lesnumeriques.com/telecharger/pc-health-check-outil-de-compatibilite-windows-11-50770-40992-telechargement)
* [WHYNOTWIN11](https://www.lesnumeriques.com/telecharger/whynotwin11-50802)
* [TrueCrypt](https://truecrypt.fr/)
* [Rufus](https://rufus.ie/fr/)
* [WinDirStat](https://windirstat.net/)
* [HDGraph](https://www.hdgraph.com/index.php?option=com_content&view=article&id=51&Itemid=56&lang=fr)
* [Recuva](https://www.ccleaner.com/recuva)
* [CCleaner](https://www.ccleaner.com/fr-fr)
* [TESTDISK & PHOTOREC](https://www.lesnumeriques.com/telecharger/testdisk-photorec-20534)
* [HxD Hex Editor](https://www.lesnumeriques.com/telecharger/hxd-hex-editor-20536)
* [VSCode](https://code.visualstudio.com/)
* [Resource Hacker](http://www.angusj.com/resourcehacker/)
* [7Zip](https://www.7-zip.org/)
* [HWiNFO](https://www.hwinfo.com/)
* [Tails](https://tails.boum.org/index.fr.html)
* [Win10 Privacy](https://www.w10privacy.de/deutsch-start/download/)
* [LockHunter](https://lockhunter.com/)
* [SuperCopier](https://www.lesnumeriques.com/telecharger/supercopier-20198)
* [Format Factory](https://www.clubic.com/telecharger-fiche223920-formatfactory.html)
* [Team Viewer](https://www.clubic.com/telecharger-fiche434102-teamviewer-portable.html)
* [Putty](https://www.putty.org/)
* [OBS](https://obsproject.com/)
* [Flashbackrecorder Express](https://www.flashbackrecorder.com/express/)

### Logiciels spécialisé / Métier

* CAO
    * Fusion 360
* Architecture
    * AutoCAD
    * Rhinoceros
* MAO
    * Cubase
    * Ableton Live
    * Logic Pro
* PAO
    * QUARKXPRESS
    * inDesign

* Video
    * Premiere
    * Resolve
* Comptablilité
    * EBP
    * Ciel
