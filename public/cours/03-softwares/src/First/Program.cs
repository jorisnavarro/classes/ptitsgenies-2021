﻿using System;


namespace First
{
    class Program
    {
        static void Main(string[] args)
        {
            HelloWorld();
            Ask();
        }

        static void HelloWorld()
        {
            Console.WriteLine("Hello, World"); 
            Console.ReadKey(true);           
        }
    
        static void Ask()
        {
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            var currentDate = DateTime.Now;
            Console.WriteLine($"{Environment.NewLine}Hello, {name}, on {currentDate:d} at {currentDate:t}!");
            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey(true);
        }       
    }
}
